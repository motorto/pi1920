#include<stdio.h>

int mdc (int a,int b){
    int r;
    while(b!=0){
        r=a%b;
        a=b;
        b=r;
    }
    return a;
}

int main (){
    int divi,num,den;
    printf ("Numerador: "); scanf ("%d",&num);
    printf ("Denominador: "); scanf ("%d",&den);
    divi=mdc(num,den);
    printf ("A fração %d/%d é equivalente a %d/%d\n",num,den,num/divi,den/divi);
    return 0;
}
