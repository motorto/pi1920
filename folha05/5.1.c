#include<stdio.h>
int primo(int num) {
    for (int d=2;d*d<=num;d++){
        if (num%d==0)
            return 0;
    }
    return 1;
}

int main () {
    int lim;
    printf ("Insira o limite: "); scanf ("%d",&lim);
    for(int i=2;i<lim;i++){
        if (primo(i)){
            printf ("%d ",i);
        }
    }
    printf ("\n");
    return 0;
}
