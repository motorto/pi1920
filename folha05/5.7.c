#include<stdio.h>
#include<ctype.h>
int main () {
    char ch;
    int pont=0;
    while((ch=getchar())!=EOF){
        ch=toupper(ch);
        switch(ch){
            case 'A':
            case 'E':
            case 'I':
            case 'L':
            case 'N':
            case 'O':
            case 'R':
            case 'T':
            case 'S':
            case 'U':
                pont+=1;
                break;
            case 'D':
            case 'G':
                pont+=2;
                break;
            case 'B':
            case 'C':
            case 'M':
            case 'P':
                pont+=3;
                break;
            case 'F':
            case 'H':
            case 'V':
            case 'W':
            case 'Y':
                pont+=4;
                break;
            case 'K':
                pont+=5;
                break;
            case 'J':
            case 'X':
                pont+=8;
                break;
            case 'Q':
            case 'Z':
                pont+=10;
                break;
        }
    }
    printf ("Pontuação= %d",pont);
    return 0;
}
