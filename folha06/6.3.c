#include<stdio.h>
#include<stdlib.h>
int main (){
    int numero=rand()%1000+1,tentativas=0,resposta;
    printf ("Descubra o valor compreendido entre 1 e 1000!\n");
    do {
	scanf ("%d",&resposta);
	if (resposta<numero){
	    printf ("Demasiado Baixo!\n");
	    tentativas++;
	}
	else if(resposta>numero){
	    printf ("Demasiado Alto!\n");
	    tentativas++;
	}
    }while (resposta!=numero);
    printf ("Acertou em %d tentativas!\n",tentativas);
    return 0;
}
