#include<stdio.h>
#define SIZE 10
int repetidos(int vec[],int vlido){
	for (int i=0;i<SIZE;i++){
		if (vec[i] == vlido)
			return 1; 
	}
	return 0;
}

void escrevervec (int vec[]) {
	for (int i=0;i<SIZE;i++){
		printf ("%d ",vec[i]);
	}
	printf ("\n");
}


int main (){
    int vec[SIZE]= {0} ,posicao=0,vlido;
    do {
	    scanf ("%d",&vlido);
	    if (repetidos(vec,vlido)==0){
		    vec[posicao]=vlido;
		    posicao++;
	    }
    } while (vlido!=(-1));
    escrevervec(vec);
    return 0;
}
