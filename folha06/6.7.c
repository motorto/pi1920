#include<string.h>
#include<stdio.h>
#include<ctype.h>

int main () {
	char ch,input[26]={0};

	while ((ch=getchar())!='\n'){
		if(isalpha(ch)){
			ch = toupper(ch);
			ch = ch -'A';
			input[ch]++;
		}
	}

	for (int i=0;i<26;i++){
		printf ("%c: %d\n",i+'A',input[i]);
	}
	return 0;
}
