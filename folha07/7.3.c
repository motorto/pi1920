#include<stdio.h>
#include<ctype.h>
#define SIZE 5
int letras (char a[]){
	for (int i=0;i<SIZE;i++){
		if (!isalpha(a[i])){
			return 0;
		}
	}
	return 1;
}

int main (){
	char a[SIZE];
	for (int i=0;i<SIZE;i++){
		scanf ("%c",&a[i]);
	}
	if (letras(a)){
		printf ("OK\n");
	}
	else 
		printf ("NOPE\n");
	return 0;
}
