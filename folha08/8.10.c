#include<stdio.h>
#include<assert.h>
int decimal (char str[]){
	int m=10, res=str[0]-'0';
	for (int i=1;str[i]!='\0';i++){
		res*=m;
		res+=(str[i]-'0');
	}
	return res;
}

int main () {
	char str[]={"1234"};
	for (int i=0;str[i]!='\0';i++){
		int temp=str[i]-'0';
		assert (temp > 0 && temp < 9 );
	}
	printf ("%d\n",decimal (str));
	return 0;
}
