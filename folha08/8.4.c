#include<stdio.h>
void selecsort(int vec[],int size){
	int i,j;
	for (i=0;i<size;i++){
		int imin=i;
		for (j=i+1;j<size;j++){
			if (vec[j]<vec[imin])
				imin=j;
		}
		if (imin!=i){
			int temp=vec[i];
			vec[i]=vec[imin];
			vec[imin]=temp;
		}
	}
}

void insert_sort(int vec[],int size){
	int i,j;
	for (i=1;i<size;i++){
		int x = vec[i];
		j=i-1;
		while (j>=0 && vec[j] > x){
			vec[j+1] = vec[j];
			j--;
		}
		vec[j+1] = x;
	}
}

int main () {
	int vec[]={3,1,2,2,4,0};
	int size=6;
//	selecsort(vec,size);
	insert_sort(vec,size);
	for (int i=0;i<size-1;i++){
		printf ("%d\n",vec[i]);
	}
	return 0;
}
