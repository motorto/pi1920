#include<stdio.h>
void selection_sort_decrescente(int vec[],int size){
	int i,j;
	for (i=0;i<size;i++){
		int imax=i;
		for(j=i+1;j<size;j++){
			if (vec[j]>vec[imax])
				imax=j;
		}
		if (imax!=i){
			int tmp=vec[i];
			vec[i]=vec[imax];
			vec[imax]=tmp;
		}
	}
}


int main () {
	int vec[]={3,1,2,2,0,4};
	int size=6;
	selection_sort_decrescente(vec,size);
	for (int i=0;i<size-1;i++){
		printf ("%d",vec[i]);
	}
	return 0;
}
