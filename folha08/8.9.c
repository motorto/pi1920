#include<stdio.h>
#include<assert.h>

int max (int a,int b, int c) {
	if (a>=b && a>=c) 
		return a;
	else if (b>a && b>c)
		return b;
	else
		return c;
}

int min (int a,int b ,int c) {
	if (a<=b && a<=c)
		return a;
	else if (b<=a && b<=c)
		return b;
	else
		return c;
}

int main (void) {
	int a,b,c;
	scanf ("%d %d %d",&a,&b,&c);
	int mediana = a+b+c-min(a,b,c)-max(a,b,c);
	assert (min (a,b,c) <= mediana && mediana <= max (a,b,c));
	printf ("%d\n",mediana);
	return 0;
}
