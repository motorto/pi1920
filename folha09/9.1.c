#include<stdio.h>
#include<string.h>

int partition (char vec[],int l,int u){
	int i,m,temp;
	m=l;
	for (i=l+1;i<=u;i++){
		if(vec[i]<vec[l]){
			m++;
			temp=vec[i];
			vec[i]=vec[m];
			vec[m]=temp;
		}
	}
	temp=vec[l];
	vec[l]=vec[m];
	vec[m]=temp;
	return m;
}


void quicksort_rec(char vec[],int l,int u){
	int m;
	if (l>=u)
		return;
	m=partition(vec,l,u);
	quicksort_rec(vec,l,m-1);
	quicksort_rec(vec,m+1,u);
}

void ordenar(char str[]) {
	quicksort_rec(str,0,strlen(str)-1);
}

int main () {
	char str[]={"ALGORITMO"};
	ordenar (str);
	printf ("%s\n",str);
	return 0;
}
