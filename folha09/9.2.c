#include<ctype.h>
#include<string.h>
#include<stdio.h>

int comparesize(char str1[],char str2[]){
    int a=strlen(str1); int b=strlen(str2);
    if (a==b){
        return 1;
    }
    else
        return 0;
}

void insert (char vec[],int n){ // Insertion Sort (Ordena a string)
	int i,j;
	for (i=1;i<n;i++){
		int x=vec[i];
		j=i-1;
		while (j>=0 && vec[j]>x){
			vec[j+1]=vec[j];
			j--;
		}
		vec[j+1]=x;
	}
}

void minus(char str[]){  // Meter String em Minusculas;
	for (int i=0;str[i]!='\0';i++){
		str[i]=tolower(str[i]);
	}
}

int anagramas(char str1[],char str2[]) {
	insert(str1,strlen(str1)); minus(str1);
	insert(str2,strlen(str2)); minus (str2);	
	int size=comparesize(str1,str2);
	if (size ==0)
		return 0;
	for (int i=0;i<strlen(str1);i++){
		if (str1[i]!=str2[i])
			return 0;
	}
	return 1;
}

int main () {
	char str1[]={"a"};
	char str2[]={""};
	int r= anagramas (str1,str2);
	printf ("%d\n",r);
	return 0;
}
