#include<stdio.h>
#include<string.h>
#include<ctype.h>

int particao (char vec[],int l,int u) {
	int i,m,temp;
	m=l;
	for (i=l+1;i<=u;i++){
		if (vec[i]< vec[l])
		{
			m++;
			temp=vec[i];
			vec[i]=vec[m];
			vec[m]=temp;
		}
	}
	temp=vec[l];
	vec[l]=vec[m];
	vec[m]=temp;
	return m;
}


void quicksort (char vec[],int a,int b){
	int c;
	if (a>=b)
		return;
	c=particao(vec,a,b);
	quicksort(vec,a,c-1);
	quicksort(vec,c+1,b);
}

void normalizar (char a[],int size){  // esta função mete a string em minuscula e mete os chars a esquerda
	int k=0;
	for (int i=0;i<size;i++){
		if(isalpha(a[i])){
			a[k]=a[i];  
			a[k]=tolower(a[k]);
			k++;
		}
	}
	a[k]='\0';
}

int comparesize (char a[],char b[]){
	if (strlen(a)==strlen(b))
		return 0;
	else
		return 1;
}

int anagrama (char a[],char b[]){
	if (comparesize(a,b)==0)
		return 0;
	for (int i=0;i<strlen(a);i++){
		if(a[i]!=b[i])
			return 0;
	}
	return 1;
}
			       
int main (void) {
	char a[]={"est=veritas ??"};
	char b[]={"Est vir qui adest"};
	normalizar (a,strlen(a));normalizar (b,strlen(b));
	quicksort (a,0,strlen(a)-1); quicksort (b,0,strlen(b)-1);
	printf ("%s\n",a);
	printf ("%s\n", b);
	printf ("%d\n",anagrama (a,b));
	return 0;
}
