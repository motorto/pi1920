#include<stdio.h>
#define N 3

int soma_diagonal(int mat[N][N]) {
	int soma=0;
	for (int i=0;i<N;i++){
		for (int j=0;j<N;j++){
			if (i==j)
				soma+=mat[i][j];
		}
	}
	return soma;
}

int soma_diagonal2(int mat[N][N]){
	int soma=0;
	for (int i=0;i<N;i++){
		for (int j=0;j<N;j++){
			if (i+j==N-1){
				soma+=mat[i][j];
			}
		}
	}
	return soma;
}

int main () {
	int matrix[N][N]={{2,5,8},{1,3,2}};
	printf ("A Soma da diagonal principal é: %d\n",soma_diagonal(matrix));
	printf ("A Soma da diagonal secundaria é: %d\n",soma_diagonal2(matrix));
	return 0;
}
