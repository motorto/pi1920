#include<stdio.h>
void max_min(int vec[],int size,int *pmax,int *pmin){
	*pmax=vec[0],*pmin=vec[0];
	for (int i=1;i<size;i++){
		if(*pmax<vec[i])
			*pmax=vec[i];
		else if(*pmin>vec[i])
			*pmin=vec[i];
	}
}


int main (){
	int vec[]={30,2,-1,31,4,7};
	int size=6;
	int maior,menor;
	max_min(vec,size,&maior,&menor);
	printf ("%d\t%d\n",maior,menor);
	return 0;
}
